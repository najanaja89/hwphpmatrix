<?php

class Matrix
{
    protected $matrix;

    public function __construct(array $matrix = [])
    {
        $this->matrix = $matrix;
    }

    public function add(Matrix $matrix)
    {
        for ($i = 0; $i < count($this->matrix); $i++) {
            for ($k = 0; $k < count($this->matrix[$i]); $k++) {

                $this->matrix[$i][$k] += $matrix->toArray()[$i][$k];
            }
        }
    }

    public function diff(Matrix $matrix)
    {
        for ($i = 0; $i < count($this->matrix); $i++) {
            for ($k = 0; $k < count($this->matrix[$i]); $k++) {

                $this->matrix[$i][$k] -= $matrix->toArray()[$i][$k];
            }
        }
    }

    public function mult(Matrix $matrix)
    {
        $cols = self::countCols($this->matrix);
        $rows = self::countRows($matrix->toArray());
        if ($rows == $cols) {
            $result = array();
            for ($i = 0; $i < count($this->matrix); $i++) {
                for ($j = 0; $j < count($matrix->toArray()[0]); $j++) {
                    $result[$i][$j] = 0;
                    for ($k = 0; $k < count($matrix->toArray()); $k++) {
                        $result[$i][$j] += $this->matrix[$i][$k] * $matrix->toArray()[$k][$j];
                    }
                }
            }
            $this->matrix=$result;
        }
        else die("Incompatible matrices");
    }

    public function toArray()
    {
        return $this->matrix;
    }

    private function countRows(array $matrix)
    {
        $row = 0;
        for ($i = 0; $i < count($matrix); $i++) {
            $row++;
        }
        return $row;
    }

    private function countCols(array $matrix)
    {
        $col = 0;
        for ($i = 0; $i < count($matrix); $i++) {
            for ($k = 0; $k < count($matrix[$i]); $k++) {
                if ($i == 0) $col++;
            }
        }
        return $col;
    }
}